import './App.css';
import TranslateForm from './components/TranslateForm'
function App() {
  return (
    <div className="App">
      <TranslateForm/>
    </div>
  );
}

export default App;
