import React, { Component } from 'react'
import swal from 'sweetalert';
import axios from 'axios'
import { Portaddress } from '../Portaddress'
import ReactLoading from 'react-loading';

export class TranslateForm extends Component {
    state = {
        inputType: 'textarea',
        textarea: true,
        textfile: false,
        textAreaData: '',
        selectedFile: null,
        loading: false,
        gender:"MALE",
    }
    componentDidMount() {
        this.setState({ showView: false })
        this.deleteSession()
        sessionStorage.setItem("randomUser", Math.floor(Math.random() * 10000));
        window.addEventListener('beforeunload', this.keepOnPage);
    }
    componentWillUnmount() {
        window.removeEventListener('beforeunload', this.keepOnPage);
    }
    keepOnPage = (e) => {
        var message = 'Warning!\n\nYour Current data will get erassed!!!.';
        e.returnValue = message;
        return message;
    }
    deleteSession = () => {
        const query = { 'id': sessionStorage.getItem("randomUser") }
        axios.post(`${Portaddress.server}/deletesession`, { query })
            .then((response) => {
                console.log(response.status)
            }).catch(err => {
                console.log(err.response)
            })
    }
    handleChange = (e) => {
        this.setState({ inputType: e })
        // console.log(this.state.inputType)
        if (this.state.inputType === 'textarea') {
            this.setState({ textfile: !this.state.textfile })
            this.setState({ textarea: !this.state.textarea })
        }
        else {
            this.setState({ textfile: !this.state.textfile })
            this.setState({ textarea: !this.state.textarea })
        }

    }
    confirmAlert = () => {
        swal({
            text: "Convert text to voice ?",
            dangerMode: false,
            buttons: ['Cancel', 'Ok']
        })
            .then(ok => {
                if (ok) {
                    this.sendRequest()
                }
            });
    }

    downloadAlert = (audioName) => {
        swal({
            text: "Download voice clip ?",
            dangerMode: false,
            buttons: ['Cancel', 'Download']
        })
            .then(Download => {
                if (Download) {
                    this.downloadAudio(audioName)
                }
            });
    }

    onTextChange = (e) => {
        this.setState({ textAreaData: e.target.value })
    }
    onFileSelect = (e) => {
        this.setState({ selectedFile: e.target.files[0] })
    }
    onSubmit = () => {
        console.log(this.state.inputType)
        console.log(this.state.textAreaData)
        this.confirmAlert()
    }
    downloadAudio = (audioName) => {
        const userId = sessionStorage.getItem("randomUser")
        fetch(`${Portaddress.server}/download/` + String(userId) + '&' + audioName, {
            responseType: 'blob',
            headers: {
                'Content-Type': 'multipart/form-data',
            }
        })
            .then(response => response.blob())
            .then(blob => {
                // this.setState({isActive:false})
                // this.swalAlert()
                const url = window.URL.createObjectURL(new Blob([blob]))
                const a = document.createElement('a')
                a.href = url
                a.setAttribute('download', audioName)
                document.body.appendChild(a)
                a.click()
                a.parentNode.removeChild(a)
                // this.setState({isActive:true})
                // this.setState({gender:'MALE'})
                // this.setState({selectedFile:null})
            })
    }
    sendRequest = () => {
        if (this.state.inputType == 'textarea') {
            const textData = { text: this.state.textAreaData, 'sessionUser': sessionStorage.getItem("randomUser"), 'gender':this.state.gender,}
            axios.post(`${Portaddress.server}/textdatatospeech`, textData)
                .then((response) => {
                    console.log(response)
                    if (response.status == 200) {
                        this.downloadAlert(response.data.audioName)
                    }
                })
                .catch(err => {
                    console.log(err)
                    if (err.response.status === 500) {
                        swal({
                            text: "Invalid text to convert !!",
                            dangerMode: true,
                            icon: "warning",
                            buttons: false,
                        })
                    }
                })
        }
        else {
            this.setState({ 'loading': true })
            // console.log(this.state.selectedFile.name)
            const formData = new FormData();
            if ((this.state.selectedFile)) {
                formData.append('file', this.state.selectedFile, this.state.selectedFile.name);
                formData.append('sessionUser', sessionStorage.getItem("randomUser"))
                formData.append('gender',this.state.gender)
                console.log(this.state.selectedFile.name)
            }
            console.log(formData)
            axios.post(`${Portaddress.server}/textfiletospeech`, formData)
                .then((response) => {
                    console.log(response)
                    this.setState({ 'loading': false })
                    if (response.status == 200) {
                        this.downloadAlert(response.data.audioName)
                    }
                })
                .catch(err => {
                    console.log(err)
                    this.setState({ 'loading': false })
                    if (err.response.status === 500) {
                        swal({
                            text: "Invalid input file, please uplaod text file only",
                            dangerMode: true,
                            icon: "warning",
                            buttons: false,
                        })
                    }
                })
        }
    }
    setGender(event) {
        console.log(event.target.value);
        this.setState({gender:event.target.value})
      }

    render() {
        const loading = this.state.loading
        return (
            <div>
                <div className="container">
                    <div className="row mt-5">
                        <div className="col-md-4 mt-5">
                            <span className=" float-md-right">
                                <input className="mr-2" type="checkbox" checked={this.state.textarea} id="textarea_cb" value="textarea" onChange={() => this.handleChange("textarea")} />
                                <label for="textarea_cb">text area</label>
                            </span>
                        </div>
                        <div className="col-md-6">
                            <textarea className="ml-5 mt-2" disabled={this.state.textfile} type="textarea" rows="5" cols="50" onChange={this.onTextChange}></textarea>
                        </div>
                    </div>
                    <div className="row mt-2">
                        <div className="col-md-4">
                            <span className=" float-md-right">
                                <input className="mr-3" type="checkbox" checked={this.state.textfile} id="textfile_cb" value="textfile" onChange={() => this.handleChange('textfile')} />
                                <label for="textfile_cb">text file</label>
                            </span>
                        </div>
                        <div className="col-md-6">
                            <input type="file" id='file' name='file' onChange={this.onFileSelect} disabled={this.state.textarea}></input>
                        </div>
                    </div>
                    <div className="row mt-2">
                        <div className="col-md-4">
                            <span className=" float-md-right">
                                <label for="textfile_cb">Gender</label>
                            </span>
                        </div>
                        <div className="col-md-4">
                            <div onChange={this.setGender.bind(this)}>
                                <input className="" type="radio" value="MALE" name="gender" checked={this.state.gender==="MALE"}/> Male
                                <input className="ml-3" type="radio" value="FEMALE" name="gender" checked={this.state.gender==="FEMALE"}/> Female
                            </div>
                        </div>
                    </div>
                    <div className="row mt-5">
                        <div className="col-md-12">
                            <input type="button" disabled={loading} onClick={this.onSubmit} value="Convert to Voice" />
                        </div>
                    </div>
                    <div className="row mt-5">
                        <div className="col-md-12 justify-content-center">
                            {loading
                                ? <div>
                                    <div className="col-md-7 pl-2 offset-md-5">
                                        <ReactLoading className="" type="balls" color="red" height={5} width={100} />
                                    </div>
                                </div>
                                : <span></span>
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default TranslateForm
